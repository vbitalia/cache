<?php
/**
 * PSR-6 Cache Item Pool.
 *
 * The primary purpose of CachePool is to accept a key from the Calling
 * Library and return the associated CacheItemInterface object. It is also the
 * primary point of interaction with the entire cache collection. All
 * configuration and initialization of the Pool is left up to the Implementing
 * Library.
 *
 * @package VB\Cache
 * @copyright 2015 VB Italia Srl
 * @since 0.1.0
 */

namespace VB\Cache;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\CacheException;
use Doctrine\Common\Cache\CacheProvider;
use VB\Logger\Logger;

/**
 * CacheItemInterface objects generator.
 *
 * @package VB\Cache
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.1.0
 * @version 1.2.0
 */
class CachePool implements CacheItemPoolInterface {
  /**
   * Cache Pool Items.
   *
   * @since 1.0.0
   * @access private
   * @var array
   */
  private $items = array();

  /**
   * Cache items to be persisted later.
   *
   * @since 1.0.0
   * @access private
   * @var array
   */
  private $deferred = array();

  /**
   * Cache Provider.
   *
   * This MUST be compatible with Doctrine\Common\Cache\CacheProvider.
   *
   * @since 1.0.0
   * @access private
   * @var object
   */
  private $provider;

  /**
   * Cache Pool namespace.
   *
   * @since 1.1.0
   * @access private
   * @var string
   */
   private $namespace = 'vb';

  /**
   * Class constructor.
   *
   * @since 1.0.0
   * @access public
   *
   * @param string $adapter Cache adapter name. E.g. "Apc", "Redis", "MongoDB".
   *
   * @throws InvalidArgumentException if $adapter is not a string.
   * @throws RuntimeException if the adapter is invalid.
   */
  public function __construct($adapter) {
    try {
      if (!is_string($adapter))
        throw InvalidArgumentException::typeMismatch('adapter', $adapter, 'String');

      // We support only Doctrine Cache providers. Check if the given adapter is
      // available.
      $fqdn = sprintf('\Doctrine\Common\Cache\%sCache', $adapter);
      if (!class_exists($fqdn))
        throw new RuntimeException(sprintf('Provider %s does not exist', $adapter));

      $provider = new $fqdn;
      // Check if the given provider is compatible with Doctrine CacheProvider.
      if (!is_a($provider, '\Doctrine\Common\Cache\CacheProvider'))
        throw new RuntimeException(sprintf('Provider %s is not compatible with this Pool', $adapter));

      $this->provider = $provider;
      $this->setNamespace($this->namespace);
    } catch (CacheException $e) {
      $logger = new Logger;
        $logger->notice($e->getMessage());
    }
  }

  /**
   * Class destructor.
   *
   * @since 1.1.0
   * @access public
   */
  public function __destruct() {
    $this->commit();
  }

  /**
   * Set Cache Pool namespace.
   *
   * @since 1.1.0
   * @access public
   *
   * @param string $namespace Cache Pool namespace.
   * @return self
   *
   * @throws InvalidArgumentException if $namespace is not a string.
   */
  public function setNamespace($namespace) {
    try {
      if (!is_string($namespace))
        throw InvalidArgumentException::typeMismatch('namespace', $namespace, 'String');

      $this->namespace = $namespace;
      $this->provider->setNamespace($namespace);
      return $this;
    } catch (CacheException $e) {
      $logger = new Logger;
      $logger->notice($e->getMessage());
    }
  }

  /**
   * Return Cache Item representing the specified key.
   *
   * This method must always return a CacheItemInterface object, even in case
   * of a cache miss. It MUST NOT return null.
   *
   * @since 1.2.0 Raise an exception if key is not a legal value.
   * @since 1.0.0
   * @access public
   *
   * @param string $key The key for which to return the corresponding cache item.
   * @return CacheItemInterface The corresponding Cache Item.
   *
   * @throws InvalidArgumentException if the $key string is not a legal value.
   */
  public function getItem($key) {
    try {
      if (!array_key_exists($key, $this->items))
        $this->items[$key] = new CacheItem($key, $this->provider);

      return $this->items[$key];
    } catch (CacheException $e) {
      $logger = new Logger;
      $logger->notice($e->getMessage());
      throw $e;
    }
  }

  /**
   * Return traversable set of cache items.
   *
   * @since 1.2.0 Raise an exception if key is not a legal value.
   * @since 1.0.0
   * @access public
   *
   * @param array $keys An indexed array of keys of items to retrieve.
   * @return array|\Traversable A traversable collection of Cache Items keyed
   * by the cache keys of each item. A cache item will be returned for each key,
   * even if that key is not found. However, if no keys are specified then an
   * empty traversable MUST be returned instead.
   *
   * @throws InvalidArgumentException if any of the keys in $keys are not a
   * legal value.
   */
  public function getItems(array $keys = array()) {
    // Return empty array of no keys are specified.
    if (empty($keys))
      return array();

    foreach ($keys as $key)
      if (!self::keyIsLegal($key))
        throw new InvalidArgumentException(sprintf(
          'Key passed to %s is not a legal value.',
          __METHOD__
        ));

    $items = array();
    foreach ($keys as $key)
        $items[$key] = $this->getItem($key);

    return $items;
  }

  /**
   * Confirm if the cache contains specified cache item.
   *
   * Note: This method MAY avoid retrieving the cached value for performance
   * reasons. This could result in a race condition with
   * `CacheItemInterface::get()`. To avoid such situation use
   * `CacheItemInterface::isHit()` instead.
   *
   * @since 1.2.0 Raise an exception if key is not a legal value.
   * @since 1.0.0
   * @access public
   *
   * @param string $key The key for which to check existence.
   * @return bool True if the item exists in the cache, false otherwise.
   *
   * @throws InvalidArgumentException if the $key string is not a legal value.
   */
  public function hasItem($key) {
    if (!self::keyIsLegal($key))
      throw new InvalidArgumentException(sprintf(
        'Key passed to %s is not a legal value.',
        __METHOD__
      ));

    return $this->provider->contains($key);
  }

  /**
   * Delete all items in the pool.
   *
   * @todo This SHOULD remove all items from the cache, not every `$this->items`.
   *
   * @since 1.0.0
   * @access public
   *
   * @return bool True if the pool was successfully cleared. False if there was
   * an error.
   */
  public function clear() {
    return $this->provider->deleteAll();
  }

  /**
   * Remove the item from the pool.
   *
   * @since 1.2.0 Raise an exception if key is not a legal value.
   * @since 1.1.0
   * @access public
   *
   * @param string $key The key for which to delete.
   * @return bool True if the item was successfully removed. False if there was
   * an error.
   *
   * @throws RuntimeException if the item could not be deleted.
   * @throws InvalidArgumentException if the $key string is not a legal value.
   */
  public function deleteItem($key) {
    if (!self::keyIsLegal($key))
      throw new InvalidArgumentException(sprintf(
        'Key passed to %s is not a legal value.',
        __METHOD__
      ));

    try {
      if (!$this->getItem($key)->delete())
        throw new RuntimeException(sprintf('Item identified by %s could not be removed', $key));
      unset($this->items[$key]);
    } catch (RuntimeException $e) {
      $logger = new Logger;
      $logger->notice($e->getMessage());
      return false;
    }

    return true;
  }

  /**
   * Remove multiple items from the pool.
   *
   * @since 1.2.0 Raise an exception if key is not a legal value.
   * @since 1.0.0
   * @access public
   *
   * @param array $keys An array of keys that should be removed from the pool.
   * @return bool True if the items were successfully removed. False if there
   * was an error.
   *
   * @throws InvalidArgumentException if any of the keys in $keys are not a
   * legal value.
   */
  public function deleteItems(array $keys = array()) {
    if (empty($keys))
      return true;

    foreach ($keys as $key)
      if (!self::keyIsLegal($key))
        throw new InvalidArgumentException(sprintf(
          'Key passed to %s is not a legal value.',
          __METHOD__
        ));

    // Declare array of delete errors.
    $errors = array();

    foreach ($keys as $key) {
      try {
        if (!$this->deleteItem($key))
          throw new RuntimeException($key);
      } catch (CacheException $e) {
        $errors[] = $e->getMessage();
      }
    }

    // Return false if there were any errors.
    if (!empty($errors)) {
      $logger = new Logger;
      $logger->notice(sprintf(
        'Cache Items identified by the following keys could not be deleted: %s',
        join(', ', $errors)
      ));
      return false;
    }

    return true;
  }

  /**
   * Persist cache item immediately.
   *
   * @since 1.0.0
   * @access public
   *
   * @param CacheItemInterface $item The cache item to save.
   * @return bool True if the item was successfully persisted. False if there
   * was an error.
   */
  public function save(CacheItemInterface $item) {
    try {
      if (!$item->save())
        throw new RuntimeException(sprintf('Item identified by %s could not be saved', $item->getKey()));
    } catch (CacheException $e) {
      $logger = new Logger;
      $logger->warning($e->getMessage());
      return false;
    }
    return true;
  }

  /**
   * Set cache item to be persisted later.
   *
   * @todo Handle cases when the operation fails.
   *
   * @since 1.0.0
   * @access public
   *
   * @param CacheItemInterface $item The cache item to save.
   * @return bool False if the item could not be queued or if a commit was
   * attempted and failed. True otherwise.
   */
  public function saveDeferred(CacheItemInterface $item) {
    $this->deferred[$item->getKey()] = $item;
    return true;
  }

  /**
   * Persist deferred cache items.
   *
   * @since 1.0.0
   * @access public
   *
   * @return bool True if all not-yet-saved items were successfully saved or
   * there were none. False otherwise.
   */
  public function commit() {
    if (empty($this->deferred))
      return true;

    // Declare array of commit errors.
    $errors = array();

    foreach ($this->deferred as $item) {
      try {
        if (!$this->save($item))
          throw new RuntimeException($item->getKey());
      } catch (RuntimeException $e) {
        $errors[] = $e->getMessage();
      }
    }

    // Return false if there were any errors.
    if (!empty($errors)) {
      $logger = new Logger;
      $logger->notice(sprintf(
        'Cache Items identified by the following keys could not be committed: %s',
        join(', ', $errors)
      ));
      return false;
    }

    return true;
  }

  /**
   * Check if the key is a legal value.
   *
   * The key must be a string of at least one character that uniquely identifies
   * a cached item. The key must consist of the characters `A-Z`, `a-z`, `0-9`,
   * `_` and `.` in any order in UTF-8 encoding and a length of up to 64
   * characters.
   *
   * @since 1.2.0
   * @access private
   * @static
   *
   * @param string $key The key to be checked.
   * @return bool True if the $key is a legal value, false otherwise.
   */
  private static function keyIsLegal($key) {
    return strlen($key) <= 64 && strlen($key) > 0 && !preg_match('/[^a-z0-9_\.]/i', $key);
  }
}
