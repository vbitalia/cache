<?php
/**
 * VB CachePool Tests.
 *
 * @package VB\Cache\Tests
 * @copyright 2015 VB Italia Srl
 * @since 0.1.0
 */

namespace VB\Cache\Tests;

use VB\Cache\CachePool;
use VB\Cache\CacheItem;
use Psr\Cache\CacheItemInterface;
use VB\Cache\RuntimeException;
use VB\Cache\InvalidArgumentException;
use Doctrine\Common\Cache\ArrayCache;
use \ReflectionProperty;
use VB\Logger\Logger;

/**
 * MockItem class.
 *
 * This class is used to mock the class `CacheItem` for tests purposes.
 *
 * @package VB\Cache\Tests
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.1.0
 * @version 1.1.1
 */
class MockItem extends CacheItem implements CacheItemInterface {
  /**
   * Stub for method `CacheItem::delete()`.
   *
   * @since 1.0.0
   * @access public
   *
   * @return bool False.
   */
  public function delete() {
    return false;
  }

  /**
   * Stub for method `CacheItem::save()`.
   *
   * @since 1.0.0
   * @access public
   *
   * @return bool False.
   *
   * @throws RuntimeException if the key is 'excp'.
   */
  public function save() {
    if ($this->getKey() == 'excp')
      throw new RuntimeException('Something went wrong.');

    return false;
  }
}

/**
 * CachePoolTest class.
 *
 * This class contains all the tests for the methods in class `CachePool`.
 *
 * @package VB\Cache\Tests
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.1.0
 * @version 1.0.0
 */
class CachePoolTest extends \PHPUnit_Framework_TestCase {
  /**
   * Test for method `__construct()`.
   *
   * This test verifies that `__construct()` correctly store the adapter in the
   * object's provider property, and set the default namespace.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::__construct
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testConstructor() {
    // Set accessibility to object's properties.
    $reflection_provider = new ReflectionProperty('VB\Cache\CachePool', 'provider');
    $reflection_provider->setAccessible(true);
    $reflection_namespace = new ReflectionProperty('VB\Cache\CachePool', 'namespace');
    $reflection_namespace->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $provider_result = $reflection_provider->getValue($cache_pool);
    $namespace_result = $reflection_namespace->getValue($cache_pool);

    $this->assertTrue(
      is_a($provider_result, 'Doctrine\Common\Cache\ArrayCache'),
      'Test failed in setting object\'s provider property.'
    );
    $this->assertTrue(
      $namespace_result == 'vb',
      'Test failed in setting object\'s default namespace property.'
    );
  }

  /**
   * Test for method `setNamespace()`.
   *
   * This test verifies that `setNamespace()` correctly set the object's
   * property.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::setNamespace
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testSetNamespace() {
    // Set accessibility to object's property.
    $reflection_namespace = new ReflectionProperty('VB\Cache\CachePool', 'namespace');
    $reflection_namespace->setAccessible(true);

    $namespace = 'test_nsp';
    $cache_pool = new CachePool('Array');
    $cache_pool->setNamespace($namespace);
    $namespace_result = $reflection_namespace->getValue($cache_pool);

    $this->assertTrue(
      $namespace == $namespace_result,
      'Test failed in setting object\'s namespace property.'
    );
  }

  /**
   * Test for method `getItem()`.
   *
   * This test verifies that `getItem()` correctly returns the CacheItem
   * corresponding to the key passed.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::getItem
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testGetItem() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array('test_key' => new CacheItem('item_key', new ArrayCache),
                   'test_key_2' => new CacheItem('item_key_2', new ArrayCache
                 ));
    $reflection_items->setValue($cache_pool, $items);
    $retrieved_item = $cache_pool->getItem('test_key');

    $this->assertEquals(
      $retrieved_item->getKey(),
      'item_key',
      'Test failed in retrieving the item corresponding to the key passed.'
    );
  }

  /**
   * Test for method `getItem()`.
   *
   * This test verifies that `getItem()` correctly returns the CacheItem
   * corresponding to the key passed, if the key does not exist.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::getItem
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testGetItemWithNonExistingKey() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array('test_key' => new CacheItem('item_key', new ArrayCache));
    $reflection_items->setValue($cache_pool, $items);
    $retrieved_item = $cache_pool->getItem('test_key_2');

    $this->assertEquals(
      $retrieved_item->getKey(),
      'test_key_2',
      'Test failed in retrieving the item corresponding to the key passed.'
    );
  }

  /**
   * Test for method `getItems()`.
   *
   * This test verifies that `getItems()` correctly returns all the CacheItem
   * corresponding to the keys passed.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::getItems
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testGetItems() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array('item_key' => new CacheItem('item_key', new ArrayCache),
                   'item_key_2' => new CacheItem('item_key_2', new ArrayCache),
                   'item_key_3' => new CacheItem('item_key_3', new ArrayCache)
                 );
    $reflection_items->setValue($cache_pool, $items);
    $retrieved_items = $cache_pool->getItems(array('item_key', 'item_key_2'));

    $this->assertEquals(
      count($retrieved_items),
      2,
      'Test failed in retrieving the items corresponding to the keys passed.'
    );
    $this->assertEquals(
      $retrieved_items['item_key']->getKey(),
      'item_key',
      'Test failed in retrieving the items corresponding to the keys passed.'
    );
    $this->assertEquals(
      $retrieved_items['item_key_2']->getKey(),
      'item_key_2',
      'Test failed in retrieving the items corresponding to the keys passed.'
    );
  }

  /**
   * Test for method `getItems()`.
   *
   * This test verifies that `getItems()` correctly returns an empty array
   * if an empty array is passed to it, or if it's called with no parameter.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::getItems
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testGetItemsWithEmptyArrayOrNoParameterPassed() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array('test_key' => new CacheItem('item_key', new ArrayCache));
    $reflection_items->setValue($cache_pool, $items);
    $retrieved_items = $cache_pool->getItems(array());
    $retrieved_items_2 = $cache_pool->getItems();

    $this->assertTrue(
      empty($retrieved_items),
      'Test failed in retrieving the items if an empty array is passed to the method.'
    );
    $this->assertTrue(
      empty($retrieved_items_2),
      'Test failed in retrieving the items if no parameter is passed to the method.'
    );
  }

  /**
   * Test for method `deleteItem()`.
   *
   * This test verifies that `deleteItem()` correctly removes the item specified
   * by the key passed from the pool.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::deleteItem
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   * @requires function ReflectionProperty::getValue
   */
  public function testDeleteItem() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array(
      'item_key' => new CacheItem('item_key', new ArrayCache),
      'item_key_2' => new CacheItem('item_key_2', new ArrayCache),
      'item_key_3' => new CacheItem('item_key_3', new ArrayCache)
    );
    $reflection_items->setValue($cache_pool, $items);
    $result = $cache_pool->deleteItem('item_key');
    $retrieved_items = $reflection_items->getValue($cache_pool);

    $this->assertTrue(
      $result,
      'Test failed in deleting the item corresponding to the key passed.'
    );
    $this->assertEquals(
      count($retrieved_items),
      2,
      'Test failed in deleting the item corresponding to the key passed.'
    );
    $this->assertFalse(
      array_key_exists('item_key', $retrieved_items),
      'Test failed in deleting the item corresponding to the key passed.'
    );
  }

  /**
   * Test for method `deleteItem()`.
   *
   * This test verifies that `deleteItem()` correctly returns false if the item
   * corresponding to the key passed was not removed from the pool.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::deleteItem
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   * @requires function ReflectionProperty::getValue
   */
  public function testDeleteItemWithError() {
    Logger::disable();

    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array(
      'item_key' => new MockItem('item_key', new ArrayCache),
      'item_key_2' => new MockItem('item_key_2', new ArrayCache),
      'item_key_3' => new MockItem('item_key_3', new ArrayCache)
    );
    $reflection_items->setValue($cache_pool, $items);
    $result = $cache_pool->deleteItem('item_key');
    $retrieved_items = $reflection_items->getValue($cache_pool);

    Logger::enable();

    $this->assertFalse(
      $result,
      'Test failed in returning false if there was an error.'
    );
    $this->assertEquals(
      count($retrieved_items),
      3,
      'Test failed in returning false if there was an error.'
    );
    $this->assertTrue(
      array_key_exists('item_key', $retrieved_items),
      'Test failed in returning false if there was an error.'
    );
  }

  /**
   * Test for method `deleteItems()`.
   *
   * This test verifies that `deleteItems()` correctly removes the items
   * specified by the keys passed from the pool.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::deleteItems
   * @requires function ReflectionProperty::setAccessible
   * @requires funciton ReflectionProperty::setValue
   * @requires funciton ReflectionProperty::getValue
   */
  public function testDeleteItems() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array(
      'item_key' => new CacheItem('item_key', new ArrayCache),
      'item_key_2' => new CacheItem('item_key_2', new ArrayCache),
      'item_key_3' => new CacheItem('item_key_3', new ArrayCache)
    );
    $reflection_items->setValue($cache_pool, $items);
    $result = $cache_pool->deleteItems(array('item_key', 'item_key_2'));
    $retrieved_items = $reflection_items->getValue($cache_pool);

    $this->assertTrue(
      $result,
      'Test failed in deleting the items correspondig to the keys passed.'
    );
    $this->assertEquals(
      count($retrieved_items),
      1,
      'Test failed in deleting the items corresponding to the keys passed.'
    );
    $this->assertTrue(
      array_key_exists('item_key_3', $retrieved_items),
      'Test failed in deleting the items correspondig to the keys passed.'
    );
  }

  /**
   * Test for method `deleteItems()`.
   *
   * This test verifies that `deleteItems()` does not remove any item from the
   * pool, if an empty array is passed to it, or if it's called with no
   * parameter.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::deleteItems
   * @requires function ReflectionProperty::setAccessible
   * @requires funciton ReflectionProperty::setValue
   * @requires funciton ReflectionProperty::getValue
   */
  public function testDeleteItemsWithEmptyArrayOrNoParameterPassed() {
    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array('test_key' => new CacheItem('item_key', new ArrayCache),
                   'test_key_2' => new CacheItem('item_key_2', new ArrayCache),
                   'test_key_3' => new CacheItem('item_key_3', new ArrayCache)
                 );
    $reflection_items->setValue($cache_pool, $items);
    $result = $cache_pool->deleteItems(array());
    $retrieved_items = $reflection_items->getValue($cache_pool);
    $result_2 = $cache_pool->deleteItems();
    $retrieved_items_2 = $reflection_items->getValue($cache_pool);
    $this->assertTrue(
      $result,
      'Test failed in deleting the items correspondig to the keys passed.'
    );
    $this->assertTrue(
      $result_2,
      'Test failed in deleting the items correspondig to the keys passed.'
    );
    $this->assertEquals(
      $retrieved_items,
      $items,
      'Test failed in deleting the items corresponding to the keys passed.'
    );
    $this->assertEquals(
      $retrieved_items_2,
      $items,
      'Test failed in deleting the items corresponding to the keys passed.'
    );
  }

  /**
   * Test for method `deleteItems()`.
   *
   * This test verifies that `deleteItems()` correctly returns false if
   * CacheItem::delete() returns false itself.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::deleteItems
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testDeleteItemsWithError() {
    Logger::disable();

    // Set accessibility to object's property.
    $reflection_items = new ReflectionProperty('VB\Cache\CachePool', 'items');
    $reflection_items->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $items = array(
      'item_key' => new MockItem('item_key', new ArrayCache),
      'item_key_2' => new MockItem('item_key_2', new ArrayCache),
      'item_key_3' => new MockItem('item_key_3', new ArrayCache)
    );
    $reflection_items->setValue($cache_pool, $items);
    $result = $cache_pool->deleteItems(array('item_key', 'item_key_2'));

    Logger::enable();

    $this->assertFalse(
      $result,
      'Test failed in returning false if there was an error.'
    );
  }

  /**
   * Test for method `save()`.
   *
   * This test verifies that `save()` correctly returns true after having saved
   * an item.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::save
   */
  public function testSave() {
    $cache_item = new CacheItem('item_key', new ArrayCache);
    $cache_pool = new CachePool('Array');
    $result = $cache_pool->save($cache_item);

    $this->assertTrue(
      $result,
      'Test failed in saving the item passed.'
    );
  }

  /**
   * Test for method `saveDeferred()`.
   *
   * This test verifies that `saveDeferred()` correctly store the item passed in
   * the deferred array.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::saveDeferred
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testSaveDeferred() {
    // Set accessibility to object's property.
    $reflection_deferred = new ReflectionProperty('VB\Cache\CachePool', 'deferred');
    $reflection_deferred->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $cache_item = new CacheItem('item_key', new ArrayCache);
    $result = $cache_pool->saveDeferred($cache_item);
    $expected_deferred = array('item_key' => $cache_item);
    $retrieved_deferred = $reflection_deferred->getValue($cache_pool);

    $this->assertTrue(
      $result,
      'Test failed in saving item in deferred property.'
    );
    $this->assertEquals(
      $expected_deferred,
      $retrieved_deferred,
      'Test failed in saving item in deferred property.'
    );
  }

  /**
   * Test for method `save()`.
   *
   * This test verifies that `save()` correctly returns false if
   * CacheItem::save() returns false itself.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::save
   */
  public function testSaveWithError() {
    Logger::disable();

    $mock_item = new MockItem('item_key', new ArrayCache);
    $cache_pool = new CachePool('Array');
    $result = $cache_pool->save($mock_item);

    Logger::enable();

    $this->assertFalse(
      $result,
      'Test failed in returning false if `CacheItem::save()` returns false itself.'
    );
  }

  /**
   * Test for method `save()`.
   *
   * This test verifies that `save()` correctly returns false if
   * CacheItem::save() throws an exception.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::save
   */
  public function testSaveWithException() {
    Logger::disable();

    $mock_item = new MockItem('excp', new ArrayCache);
    $cache_pool = new CachePool('Array');
    $result = $cache_pool->save($mock_item);

    Logger::enable();

    $this->assertFalse(
      $result,
      'Test failed in returning false if `CacheItem::save()` throws an exception.'
    );
  }

  /**
   * Test for method `commit()`.
   *
   * This test verifies that `commit()` correctly save the deferred items.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::commit
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testCommit() {
    // Set accessibility to object's property.
    $reflection_deferred = new ReflectionProperty('VB\Cache\CachePool', 'deferred');
    $reflection_deferred->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $deferred = array('item_key' => new CacheItem('item_key', new ArrayCache));
    $reflection_deferred->setValue($cache_pool, $deferred);
    $result = $cache_pool->commit();

    $this->assertTrue(
      $result,
      'Test failed in commiting deferred items.'
    );
  }

  /**
   * Test for method `commit()`.
   *
   * This test verifies that `commi()` correctly return true if there aren't
   * deferred items.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::commit
   * @requires function ReflectionProperty::setAccessible
   */
  public function testCommitWithNoDeferredItems() {
    $cache_pool = new CachePool('Array');
    $result = $cache_pool->commit();

    $this->assertTrue(
      $result,
      'Test failed in commiting deferred items.'
    );
  }

  /**
   * Test for method `commit()`.
   *
   * This test verifies that `commit()` correctly returns false if
   * CacheItem::save() returns false itself.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CachePool::commit
   */
  public function testCommitWithError() {
    Logger::disable();

    // Set accessibility to object's property.
    $reflection_deferred = new ReflectionProperty('VB\Cache\CachePool', 'deferred');
    $reflection_deferred->setAccessible(true);

    $cache_pool = new CachePool('Array');
    $deferred = array('item_key' => new MockItem('item_key', new ArrayCache));
    $reflection_deferred->setValue($cache_pool, $deferred);
    $result = $cache_pool->commit();

    Logger::enable();

    $this->assertFalse(
      $result,
      'Test failed in commiting deferred items.'
    );
  }
}
