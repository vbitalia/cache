<?php
/**
 * InvalidArgumentException class file.
 *
 * @package VB\Cache
 * @copyright 2015 VB Italia Srl
 * @since 0.1.0
 */

namespace VB\Cache;

use Psr\Cache\CacheException;
use Psr\Cache\InvalidArgumentException as psrexcp;
use VB\Common\InvalidArgumentException as InvArgExcp;

/**
 * InvalidArgumentException class.
 *
 * @package VB\Cache
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.1.0
 * @version 1.0.2
 */
class InvalidArgumentException extends InvArgExcp implements CacheException, psrexcp {}
