<?php
/**
 * RuntimeException class file.
 *
 * @package VB\Cache
 * @copyright 2015 VB Italia Srl
 * @since 0.1.0
 */

namespace VB\Cache;

use Psr\Cache\CacheException;

/**
 * RuntimeException class.
 *
 * @package VB\Cache
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.1.0
 * @version 1.0.0
 */
class RuntimeException extends \Exception implements CacheException {}
