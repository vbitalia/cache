<?php
/**
 * VB CacheItem Tests.
 *
 * @package VB\Cache\Tests
 * @copyright 2015 VB Italia Srl
 * @since 0.1.0
 */

namespace VB\Cache\Tests;

use VB\Cache\CacheItem;
use Doctrine\Common\Cache\ArrayCache;
use \ReflectionProperty;
use DateTimeInterface;
use DateTime;
use DateInterval;

/**
 * CacheItemTest class.
 *
 * This class contains all the tests for the methods in class `CacheItem`.
 *
 * @package VB\Cache\Tests
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.1.0
 * @version 1.0.0
 */
class CacheItemTest extends \PHPUnit_Framework_TestCase {
  /**
   * Test for method `__construct()`.
   *
   * This tests verifies that `__construct()` correctly set the key and the
   * provider.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::__construct
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testConstructor() {
    // Set accessibility to object's properties.
    $reflection_key = new ReflectionProperty('VB\Cache\CacheItem', 'key');
    $reflection_key->setAccessible(true);
    $reflection_provider = new ReflectionProperty('VB\Cache\CacheItem', 'provider');
    $reflection_provider->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $key_result = $reflection_key->getValue($cache_item);
    $provider_result = $reflection_provider->getValue($cache_item);

    $this->assertTrue(
      $key_result == $key,
      'Test failed in setting object\'s key property.'
    );
    $this->assertTrue(
      $provider_result == $provider,
      'Test failed in setting object\'s provider property.'
    );
  }

  /**
   * Test for method `getKey()`.
   *
   * This test verifies that `getKey()` correctly returns the key's value.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::getKey
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testGetKey() {
    // Set accessibility to object's property.
    $reflection_key = new ReflectionProperty('VB\Cache\CacheItem', 'key');
    $reflection_key->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $expected_key = $reflection_key->getValue($cache_item);
    $retrieved_key = $cache_item->getKey();

    $this->assertTrue(
      $expected_key == $retrieved_key,
      'Test failed in retrieving object\'s key property.'
    );
  }

  /**
   * Test for method `set()`.
   *
   * This test verifies that `set()` correctly set the value passed.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::set
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testSet() {
    // Set accessibility to object's property.
    $reflection_value = new ReflectionProperty('VB\Cache\CacheItem', 'value');
    $reflection_value->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $cache_item->set('test_value');
    $expected_value = $reflection_value->getValue($cache_item);

    // $this->assertTrue(
    //   $expected_value == 'test_value',
    //   'Test failed in setting object\'s value property.'
    // );
    $this->assertEquals('test_value', $expected_value, 'Failed to set object value property.');
  }

  /**
   * Test for method `get()`.
   *
   * This test verifies that `get()` correctly returns the value property.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::get
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testGet() {
    // Set accessibility to object's property.
    $reflection_value = new ReflectionProperty('VB\Cache\CacheItem', 'value');
    $reflection_value->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $reflection_value->setValue($cache_item, 'test_value');
    $cache_item->save();
    $result = $cache_item->get();

    // $this->assertTrue(
    //   $result == 'test_value',
    //   'Test failed in retrieving object\'s value property.'
    // );
    $this->assertEquals('test_value', $result, 'Failed to retrieve object value property.');
  }

  /**
   * Test for method `expiresAt()`.
   *
   * This test verifies that `expiresAt()` correctly sets the expiration time.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::expiresAt
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testExpiresAt() {
    // Set accessibility of object's property.
    $reflection_expiration = new ReflectionProperty('VB\Cache\CacheItem', 'expiration');
    $reflection_expiration->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $now = new DateTime('now');
    $cache_item->expiresAt($now);
    $expiration_result = $reflection_expiration->getValue($cache_item);

    $this->assertTrue(
      $now == $expiration_result,
      'Test failed in setting object\'s expiration property.'
    );
  }

  /**
   * Test for method `expiresAt()`.
   *
   * This test verifies that `expiresAt()` correctly sets the expiration time,
   * if no expiration time is passed to the method.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::expiresAt
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testExpiresAtWithNullParameter() {
    // Set accessibility of object's property.
    $reflection_expiration = new ReflectionProperty('VB\Cache\CacheItem', 'expiration');
    $reflection_expiration->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $interval = new DateInterval('P1Y');
    $cache_item->expiresAt(null);
    $interval_result = $reflection_expiration->getValue($cache_item);

    $this->assertTrue(
      $interval == $interval_result,
      'Test failed in setting object\'s expiration property.'
    );
  }

  /**
   * Test for method `expiresAfter()`.
   *
   * This test verifies that `expiresAfter()` correctly sets the expiration
   * time, if no expiration time is passed to the method.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::expiresAfter
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testExpiresAfterWithNullParameter() {
    // Set accessibility of object's property.
    $reflection_expiration = new ReflectionProperty('VB\Cache\CacheItem', 'expiration');
    $reflection_expiration->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $interval = new DateInterval('P1Y');
    $cache_item->expiresAfter(null);
    $interval_result = $reflection_expiration->getValue($cache_item);

    $this->assertTrue(
      $interval == $interval_result,
      'Test failed in setting object\'s expiration property.'
    );
  }

  /**
   * Test for method `expiresAfter()`.
   *
   * This test verifies that `expiresAfter()` correctly sets the expiration
   * time, if an int is passed to the method.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::expiresAfter
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testExpiresAfterWithIntParameter() {
    // Set accessibility of object's property.
    $reflection_expiration = new ReflectionProperty('VB\Cache\CacheItem', 'expiration');
    $reflection_expiration->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $interval = new DateInterval('PT30S');
    $cache_item->expiresAfter(30);
    $interval_result = $reflection_expiration->getValue($cache_item);

    $this->assertTrue(
      $interval == $interval_result,
      'Test failed in setting object\'s expiration property.'
    );
  }

  /**
   * Test for method `expiresAfter()`.
   *
   * This test verifies that `expiresAfter()` correctly sets the expiration
   * time, if a `DateInterval` is passed to the method.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::expiresAfter
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testExpiresAfterWithDateIntervalParameter() {
    // Set accessibility of object's property.
    $reflection_expiration = new ReflectionProperty('VB\Cache\CacheItem', 'expiration');
    $reflection_expiration->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $interval = new DateInterval('PT30S');
    $cache_item->expiresAfter($interval);
    $interval_result = $reflection_expiration->getValue($cache_item);

    $this->assertTrue(
      $interval == $interval_result,
      'Test failed in setting object\'s expiration property.'
    );
  }

  /**
   * Test for method `save()`.
   *
   * This test verifies that `save()` correctly returns true when the item is
   * succesfully saved.
   *
   * @since 1.0.0
   * @access public
   *
   * @requires function VB\Cache\CacheItem::save
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::setValue
   */
  public function testSave() {
    // Set accessibility of object's property.
    $reflection_value = new ReflectionProperty('VB\Cache\CacheItem', 'value');
    $reflection_value->setAccessible(true);

    $provider = new ArrayCache();
    $key = 'test_item';
    $cache_item = new CacheItem($key, $provider);
    $reflection_value->setValue($cache_item, 'item_value');
    $result = $cache_item->save();

    $this->assertTrue(
      $result,
      'Test failed in saving the item.'
    );
  }
}
